<?php

// Autentikasi HTTP
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="My API"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Authentication required.';
    exit;
} else {
    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];
    // Anda harus memverifikasi kredensial di sini

    // Contoh sederhana: Username "admin" dan Password "12345"
    if ($username !== "admin" || $password !== "12345") {
        header('HTTP/1.0 401 Unauthorized');
        echo 'Invalid credentials.';
        exit;
    }
}

// Set header untuk respons JSON
header('Content-Type: application/json');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // API List (Mengambil daftar tugas)
    $tasks = [
        // Data tugas dari database atau sumber lain
        ["id" => 1, "description" => "Tugas 1"],
        ["id" => 2, "description" => "Tugas 2"],
        // ...
    ];

    echo json_encode($tasks);
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // API Create (Membuat tugas baru)
    $data = json_decode(file_get_contents('php://input'), true);

    // Validasi data yang diterima
    if (!isset($data['description']) || empty($data['description'])) {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(["error" => "Description is required."]);
        exit;
    }

    // Simpan data tugas baru ke dalam database atau sumber lain
    // ...

    echo json_encode(["message" => "Task created successfully."]);
} elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    // API Edit (Mengedit tugas)
    $data = json_decode(file_get_contents('php://input'), true);

    // Validasi data yang diterima
    if (!isset($data['id']) || !isset($data['description'])) {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(["error" => "ID and Description are required for editing."]);
        exit;
    }

    // Simpan perubahan data tugas ke dalam database atau sumber lain
    // ...

    echo json_encode(["message" => "Task updated successfully."]);
} elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    // API Delete (Menghapus tugas)
    $data = json_decode(file_get_contents('php://input'), true);

    // Validasi data yang diterima
    if (!isset($data['id'])) {
        header('HTTP/1.0 400 Bad Request');
        echo json_encode(["error" => "ID is required for deletion."]);
        exit;
    }

    // Hapus tugas dari database atau sumber lain
    // ...

    echo json_encode(["message" => "Task deleted successfully."]);
} else {
    header('HTTP/1.0 405 Method Not Allowed');
    echo 'Method not allowed.';
}
