<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "todo_app";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Function to get tasks from the database
function getTasksFromDatabase($conn) {
    $tasks = array(); // Initialize an array to store tasks

    // Perform a query to get tasks from the database
    $result = $conn->query("SELECT id, description, created_at, completed FROM tasks");

    // Check for errors in the query
    if (!$result) {
        die("Error in SQL query: " . $conn->error);
    }

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $tasks[] = $row; // Add each task to the array
        }
    }

    return $tasks;
}

// Get tasks from the database using the function
$tasks = getTasksFromDatabase($conn);

// Close the database connection
$conn->close();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Aplikasi Tugas Sederhana</title>
</head>
<body>
    <h1>Aplikasi Tugas Sederhana</h1>

    <!-- Form untuk menambahkan tugas -->
    <form action="  .php" method="post">
        <input type="text" name="deskripsi" placeholder="Tambahkan tugas..." required>
        <button type="submit">Tambah Tugas</button>
    </form>

    <h2>Daftar Tugas</h2>

    <ul>
        <?php
        $tasks = getTasksFromDatabase();
        foreach ($tasks as $task) {
            echo '<li>';
            echo htmlspecialchars($task['deskripsi']);
            echo ' <a href="/src/edit_task.php?id=' . $task['id'] . '">Edit</a>';
            echo ' <a href="/src/mark_as_done.php?id=' . $task['id'] . '">Selesai</a>';
            echo ' <a href="/src/delete_task.php?id=' . $task['id'] . '">Hapus</a>';
            echo '</li>';
        }
        ?>
    </ul>
</body>
</html>
