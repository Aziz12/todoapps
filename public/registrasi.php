<!DOCTYPE html>
<html>
<head>
    <title>Halaman Registrasi</title>
</head>
<body>
    <h2>Registrasi</h2>
    <form action="proses_registrasi.php" method="POST">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" required>
        <br>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required>
        <br>
        <label for="password_confirm">Konfirmasi Password:</label>
        <input type="password" name="password_confirm" id="password_confirm" required>
        <br>
        <button type="submit">Registrasi</button>
    </form>
</body>
</html>
