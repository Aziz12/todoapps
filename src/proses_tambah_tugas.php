<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Validasi dan sanitasi input
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    // Handle CSRF Protection
    if (!isset($_SESSION['csrf_token']) || empty($_SESSION['csrf_token']) || $_POST['csrf_token'] !== $_SESSION['csrf_token']) {
        die("Invalid CSRF token. Request denied.");
    }

    // Masukkan tugas ke dalam database menggunakan PDO dan prepared statement
    try {
        $pdo = new PDO("mysql:host=localhost;dbname=nama_database", "username", "password");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $userId = $_SESSION['user_id']; // Anda harus menetapkan nilai ini saat pengguna login
        $stmt = $pdo->prepare("INSERT INTO tasks (user_id, description) VALUES (:user_id, :description)");
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':description', $description);
        $stmt->execute();
    } catch (PDOException $e) {
        die("Database error: " . $e->getMessage());
    }
}

// Redirect kembali ke halaman utama setelah menambahkan tugas
header("Location: index.php");
