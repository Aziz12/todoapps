<?php
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
    $taskId = $_GET["id"];

    // Periksa apakah taskId valid (misalnya, angka bulat positif)

    // Tandai tugas sebagai selesai di database (gantilah ini dengan kode yang sesuai)

    // Redirect kembali ke halaman utama
    header("Location: index.php");
}

// Redirect ke halaman utama jika tidak ada ID yang valid
header("Location: index.php");
