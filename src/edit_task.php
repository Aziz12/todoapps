<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
    // Validasi ID tugas
    $taskId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

    // Handle CSRF Protection
    if (!isset($_SESSION['csrf_token']) || empty($_SESSION['csrf_token']) || $_GET['csrf_token'] !== $_SESSION['csrf_token']) {
        die("Invalid CSRF token. Request denied.");
    }

    if ($taskId === false) {
        die("Invalid task ID.");
    }

    try {
        $pdo = new PDO("mysql:host=localhost;dbname=nama_database", "username", "password");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Query untuk mengambil tugas berdasarkan ID
        $userId = $_SESSION['user_id']; // Anda harus menetapkan nilai ini saat pengguna login
        $stmt = $pdo->prepare("SELECT * FROM tasks WHERE user_id = :user_id AND id = :id");
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':id', $taskId);
        $stmt->execute();
        $task = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($task) {
            // Tampilkan formulir pengeditan
            echo '<form action="proses_edit_task.php?id=' . $taskId . '" method="post">';
            echo '<input type="hidden" name="csrf_token" value="' . $_SESSION['csrf_token'] . '">';
            echo '<input type="text" name="description" value="' . htmlspecialchars($task['description'], ENT_QUOTES, 'UTF-8') . '" required>';
            echo '<button type="submit">Simpan Perubahan</button>';
            echo '</form>';
        } else {
            die("Task not found.");
        }
    } catch (PDOException $e) {
        die("Database error: " . $e->getMessage());
    }
}

// Redirect ke halaman utama jika tidak ada ID yang valid
header("Location: index.php");
